﻿// Christian Nilsson, AB4805
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace BipartiteMatching
{
    /// <summary>
    /// A graph with capacity and flow
    /// </summary>
    class Graph
    {
        private int numberOfVertices;
        private bool[,] adjacencyMatrix;
        private int[,] capacityMatrix;
        private int[,] flowMatrix;

        /// <summary>
        /// Constructor. Inits the matrices.
        /// </summary>
        /// <param name="numberOfVertices">Number of vertices in the graph</param>
        public Graph(int numberOfVertices)
        {
            this.numberOfVertices = numberOfVertices;

            this.adjacencyMatrix = new bool[numberOfVertices, numberOfVertices];
            this.capacityMatrix = new int[numberOfVertices, numberOfVertices];
            this.flowMatrix = new int[numberOfVertices, numberOfVertices];

            for (int i = 0; i < numberOfVertices; i++)
            {
                for(int j = 0; j < numberOfVertices; j++)
                {
                    adjacencyMatrix[i, j] = false;
                    capacityMatrix[i, j] = 0;
                    flowMatrix[i, j] = 0;
                }
            }
        }

        /// <summary>
        /// Adds undirected edge to the graph
        /// </summary>
        /// <param name="v1">A vertex</param>
        /// <param name="v2">Another vertex</param>
        public void AddUndirectedEdge(int v1, int v2)
        {
            AddEdge(v1, v2);
            AddEdge(v2, v1);
        }

        /// <summary>
        /// Removes undirected edge from the graph
        /// </summary>
        /// <param name="v1">A vertex</param>
        /// <param name="v2">Another vertex</param>
        public void RemoveUndirectedEdge(int v1, int v2)
        {
            RemoveEdge(v1, v2);
            RemoveEdge(v2, v1);
        }

        /// <summary>
        /// Adds directed edge to the graph
        /// </summary>
        /// <param name="v1">Source vertex</param>
        /// <param name="v2">Target vertex</param>
        public void AddEdge(int v1, int v2)
        {
            adjacencyMatrix[v1, v2] = true;            
        }

        /// <summary>
        /// Removes directed edge from the graph
        /// </summary>
        /// <param name="v1">Source vertex</param>
        /// <param name="v2">Target vertex</param>
        public void RemoveEdge(int v1, int v2)
        {
            adjacencyMatrix[v1, v2] = false;
        }

        /// <summary>
        /// Sets flow of an edge
        /// </summary>
        /// <param name="v1">Source vertex</param>
        /// <param name="v2">Target vertex</param>
        /// <param name="flow">Flow</param>
        public void SetFlow(int v1, int v2, int flow)
        {
            flowMatrix[v1, v2] = flow;
        }

        /// <summary>
        /// Increase or decrease flow of an edge
        /// </summary>
        /// <param name="v1">Source vertex</param>
        /// <param name="v2">Target vertex</param>
        /// <param name="capacity">How much to increase/decrease flow</param>
        public void ChangeFlow(int v1, int v2, int flow)
        {
            flowMatrix[v1, v2] += flow;
        }

        /// <summary>
        /// Gets flow of an edge
        /// </summary>
        /// <param name="v1">Source vertex</param>
        /// <param name="v2">Target vertex</param>
        /// <returns>Flow</returns>
        public int GetFlow(int v1, int v2)
        {
            return flowMatrix[v1, v2];
        }

        /// <summary>
        /// Set capacity of an edge
        /// </summary>
        /// <param name="v1">Source vertex</param>
        /// <param name="v2">Target vertex</param>
        /// <param name="capacity">Capacity</param>
        public void SetCapacity(int v1, int v2, int capacity)
        {
            capacityMatrix[v1, v2] = capacity;
        }

        /// <summary>
        /// Get capacity of an edge
        /// </summary>
        /// <param name="v1">Source vertex</param>
        /// <param name="v2">Target vertex</param>
        /// <returns>Capacity</returns>
        public int GetCapacity(int v1, int v2)
        {
            return capacityMatrix[v1, v2];
        }



        /// <summary>
        /// Get the neighbours of a vertex
        /// </summary>
        /// <param name="vertex">Vertex</param>
        /// <returns>Neighbours of vertex</returns>
        public int[] GetNeighbours(int vertex)
        {
            int[] neighbours = new int[NumberOfNeighbours(vertex)];
            int nCount = 0;

            for (int i = 0; i < numberOfVertices; i++)
            {
                if (adjacencyMatrix[vertex, i])
                {
                    neighbours[nCount] = i;
                    nCount++;
                }
            }

            //foreach(int item in neighbours) Debug.WriteLine("   Neighbour: " + item);

            return neighbours;
        }

        /// <summary>
        /// Add vertex to graph
        /// </summary>
        /// <returns>Index of new vertex</returns>
        public int AddVertex()
        {
            this.numberOfVertices++;

            bool[,] newAdjacencyMatrix = new bool[numberOfVertices, numberOfVertices];
            int[,] newCapacityMatrix = new int[numberOfVertices, numberOfVertices];
            int[,] newFlowMatrix = new int[numberOfVertices, numberOfVertices];

            // Copy the values of old matrixes to the new ones
            for (int i = 0; i < numberOfVertices; i++)
            {
                for (int j = 0; j < numberOfVertices; j++)
                {
                    if (i > (numberOfVertices - 2) || j > (numberOfVertices - 2))
                    {
                        // this is the new vertex position
                        newAdjacencyMatrix[i, j] = false;
                        newCapacityMatrix[i, j] = 0;
                        newFlowMatrix[i, j] = 0;
                    }
                    else
                    {
                        newAdjacencyMatrix[i, j] = this.adjacencyMatrix[i, j];
                        newCapacityMatrix[i, j] = this.capacityMatrix[i, j];
                        newFlowMatrix[i, j] = this.flowMatrix[i, j];
                    }
                }
            }

            this.adjacencyMatrix = newAdjacencyMatrix;
            this.capacityMatrix = newCapacityMatrix;
            this.flowMatrix = newFlowMatrix;

            return numberOfVertices-1;
        }

        /// <summary>
        /// Calculates the number of neighbours of a vertex.
        /// </summary>
        /// <param name="vertex">Vertex in graph</param>
        /// <returns>Number of neighbours</returns>
        public int NumberOfNeighbours(int vertex)
        {
            int neighbours = 0;
            for (int i = 0; i < numberOfVertices; i++)
            {
                if (adjacencyMatrix[vertex,i]) neighbours++;
            }

            return neighbours;
        }

        /// <summary>
        /// Get the adjacency matrix as string.
        /// </summary>
        /// <returns>Adjecency matrix as string</returns>
        public string getAdjacencyMatrixString()
        {
            string adjStr = "";
            for (int i = 0; i < numberOfVertices; i++)
            {
                for (int j = 0; j < numberOfVertices; j++)
                {
                    if (adjacencyMatrix[i, j]) adjStr += "1 ";
                    else adjStr += "0 ";
                }
                adjStr += Environment.NewLine;
            }

            return adjStr;
        }

        /// <summary>
        /// Get a matrix of current flow and capacity of graph, as string.
        /// </summary>
        /// <returns>Flow/capacity matrix</returns>
        public string getFlowCapacityMatrixString()
        {
            string fcStr = "";
            for (int i = 0; i < numberOfVertices; i++)
            {
                for (int j = 0; j < numberOfVertices; j++)
                {
                    fcStr += flowMatrix[i, j] + "/" + capacityMatrix[i, j] + " ";
                }
                fcStr += Environment.NewLine;
            }

            return fcStr;
        }

        /// <summary>
        /// Get the number of vertices in the graph.
        /// </summary>
        public int NumberOfVertices
        {
            get { return this.numberOfVertices; }
        }
    }
}
