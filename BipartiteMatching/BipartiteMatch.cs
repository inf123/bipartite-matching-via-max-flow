﻿// Christian Nilsson, AB4805
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace BipartiteMatching
{
    /// <summary>
    /// Methods for finding max matches in a bipartite graph.
    /// </summary>
    class BipartiteMatch
    {
        /// <summary>
        /// Finds the maximum number of matches in a bipartite graph
        /// </summary>
        /// <param name="graph">Graph to find matches in</param>
        /// <returns>Matching result (string)</returns>
        public string FindBipartiteMatches(Graph graph)
        {
            string strOut = "";

            // Check if graph is a valid biparitie graph, and divide vertices into two sets, U and V
            List<int> setU = new List<int>();
            List<int> setV = new List<int>();

            if (DivideGraph(graph, out setU, out setV))
            {
                // Convert the bipartite graph to a directed graph with sink and source.         
                int source, sink;
                Graph directedGraph = CreateDirectedGraph(graph, setU, setV, out source, out sink);

                int maxFlow = FindMaxFlow(ref directedGraph, source, sink);

                strOut += "Bipartite Matches: " + maxFlow + Environment.NewLine;

                // find matches in the bipartite graph
                foreach (int u in setU)
                {
                    int[] neighbours = graph.GetNeighbours(u);
                    foreach (int n in neighbours)
                    {
                        int flow = directedGraph.GetFlow(u, n);
                        // a flow on this edge = match!
                        if (flow > 0) strOut += "Matching edge: " + u + ", " + n + Environment.NewLine;
                    }
                }
            }
            else strOut += "Invalid Bipartite Graph." + Environment.NewLine;

            return strOut;
        }

        /// <summary>
        /// Ford-Fulkerson method for finding max flow.
        /// Augments paths with breadth first search
        /// http://en.wikipedia.org/wiki/Edmonds%E2%80%93Karp_algorithm
        /// </summary>
        /// <param name="graph">Graph to find max flow</param>
        /// <param name="source">Source vertex</param>
        /// <param name="sink">Sink vertex</param>
        /// <returns>Maximum flow</returns>
        private int FindMaxFlow(ref Graph graph, int source, int sink)
        {
            int maxFlow = 0;

            // reset flow
            for (int i = 0; i < graph.NumberOfVertices; i++)
            {
                int[] neighbours = graph.GetNeighbours(i);
                foreach (int n in neighbours)
                {
                    graph.SetFlow(i, n, 0);
                    graph.SetFlow(n, i, 0);
                }
            }

            while (true)
            {
                Graph residualGraph = CreateResidualGraph(graph);

                int[] parentTable; // path found (bfs-tree)

                int capacityOfPath = BreadthFirstSearch(residualGraph, source, sink, out parentTable);
                if (capacityOfPath == 0)
                {
                    // no more paths exist
                    break;
                }
                maxFlow += capacityOfPath;
               
                int currentVertex = sink;

                // backtrack from sink to source and update flow
                while (currentVertex != source)
                {
                    int currentParent = parentTable[currentVertex];
                    graph.ChangeFlow(currentParent, currentVertex, capacityOfPath);
                    graph.ChangeFlow(currentVertex, currentParent, -capacityOfPath);

                    currentVertex = currentParent;
                }
            }

            /*Debug.WriteLine("Adjacency Matrix of Flow Network:");
            Debug.WriteLine(graph.getAdjacencyMatrixString());
            Debug.WriteLine("Flow/Capacity Matrix of Flow Network:");
            Debug.WriteLine(graph.getFlowCapacityMatrixString());*/

            return maxFlow;
        }

        /// <summary>
        /// Find a path from source to sink if there is capacity left
        /// </summary>
        /// <param name="graph">Graph to find path in</param>
        /// <param name="source">Source vertex</param>
        /// <param name="sink">Sink vertex</param>
        /// <param name="parentTable">BFS tree</param>
        /// <returns>Capacity of path</returns>
        private int BreadthFirstSearch(Graph graph, int source, int sink, out int[] parentTable)
        {
            int[] capacityOfPath = new int[graph.NumberOfVertices];  // M
            parentTable = new int[graph.NumberOfVertices]; // P

            Queue<int> queue = new Queue<int>();

            for (int i = 0; i < graph.NumberOfVertices; i++)
            {
                parentTable[i] = -1;
            }

            // Start with source-vertex
            queue.Enqueue(source);
            capacityOfPath[source] = int.MaxValue;
            parentTable[source] = -2;

            while (queue.Count > 0)
            {
                int t = queue.Dequeue();

                int[] neighbours = graph.GetNeighbours(t);
                foreach (int n in neighbours)
                {
                    int c = graph.GetCapacity(t, n);
                    int f = graph.GetFlow(t, n);

                    if ((c - f > 0) && parentTable[n] == -1) // if there is capacity left and vertex is unvisited.
                    {
                        parentTable[n] = t;  // set parent of this vertex
                        capacityOfPath[n] = FindMinimum(capacityOfPath[t], c - f);

                        if (n != sink) queue.Enqueue(n);
                        else return capacityOfPath[sink];
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// Returns the minimum of two numbers
        /// </summary>
        /// <param name="number1">Number 1</param>
        /// <param name="number2">Number 2</param>
        /// <returns>Smallest number of number 1 & number 2</returns>
        private int FindMinimum(int number1, int number2)
        {
            if (number1 < number2) return number1;
            else return number2;
        }

        /// <summary>
        /// Create residual network
        /// </summary>
        /// <param name="graph">Bipartite graph</param>
        /// <returns>Residual network</returns>
        private Graph CreateResidualGraph(Graph graph)
        {
            Graph residualGraph = new Graph(graph.NumberOfVertices);

            for (int i = 0; i < residualGraph.NumberOfVertices; i++)
            {
                foreach (int neighbour in graph.GetNeighbours(i))
                {
                    int c = graph.GetCapacity(i, neighbour);
                    int f = graph.GetFlow(i, neighbour);

                    int newCapacity = c - f;

                    if (newCapacity > 0)
                    {
                        residualGraph.AddEdge(i, neighbour);
                        residualGraph.SetCapacity(i, neighbour, newCapacity);
                    }
                }

            }

            /*Debug.WriteLine("Adjacency Matrix of Residual Graph:");
            Debug.WriteLine(residualGraph.getAdjacencyMatrixString());
            Debug.WriteLine("Flow/Capacity Matrix Of Residual Graph:");
            Debug.WriteLine(residualGraph.getFlowCapacityMatrixString());*/

            return residualGraph;
        }

        /// <summary>
        /// Creates a directed graph with source and sink from a valid bipartite graph
        /// </summary>
        /// <param name="graph">Bipartite graph</param>
        /// <param name="setU">Nodes in set U</param>
        /// <param name="setV">Nodes in set V</param>
        /// <returns>Directed graph</returns>
        private Graph CreateDirectedGraph(Graph graph, List<int> setU, List<int> setV, out int sourceVertex, out int sinkVertex)
        {
            // Create a directed graph with source and sink from the bipartite graph.
            Graph directedGraph = new Graph(graph.NumberOfVertices);

            sourceVertex = directedGraph.AddVertex();
            sinkVertex = directedGraph.AddVertex();

            foreach (int uVertex in setU)
            {
                //Debug.WriteLine(uVertex);
                directedGraph.AddEdge(sourceVertex, uVertex);
                directedGraph.SetCapacity(sourceVertex, uVertex, 1);
                directedGraph.SetFlow(sourceVertex, uVertex, 0);

                foreach (int neighbour in graph.GetNeighbours(uVertex))
                {
                    // Add a directed edge to the new directedGraph
                    directedGraph.AddEdge(uVertex, neighbour);
                    directedGraph.SetCapacity(uVertex, neighbour, 1);
                    directedGraph.SetFlow(uVertex, neighbour, 0);
                }
            }

            foreach (int vVertex in setV)
            {
                directedGraph.AddEdge(vVertex, sinkVertex);
                directedGraph.SetCapacity(vVertex, sinkVertex, 1);
                directedGraph.SetFlow(vVertex, sinkVertex, 0);
            }

            Debug.WriteLine("Adjacency Matrix of Directed Graph (with source and sink):");
            Debug.WriteLine(directedGraph.getAdjacencyMatrixString());
            Debug.WriteLine("Flow/Capacity Matrix Of Directed Graph:");
            Debug.WriteLine(directedGraph.getFlowCapacityMatrixString());

            return directedGraph;
        }

        /// <summary>
        /// Divide bipartite graph into two sets, U and V
        /// http://en.wikipedia.org/wiki/Breadth-first_search#Pseudocode
        /// </summary>
        /// <param name="graph">Graph to divide</param>
        /// <returns>True if successful</returns>
        private bool DivideGraph(Graph graph, out List<int> setU, out List<int> setV)
        {
            setU = new List<int>();
            setV = new List<int>();

            Queue<int> queue = new Queue<int>();

            bool[] visited = new bool[graph.NumberOfVertices];
            int[] inSet = new int[graph.NumberOfVertices]; // 1 = vertex is in set U, 2 = vertex is in set V

            // set defaults (behövs kanske inte..)
            for (int i = 0; i < graph.NumberOfVertices; i++)
            {
                inSet[i] = 0;
                visited[i] = false;
            }

            // Start with vertex 0
            queue.Enqueue(0);
            visited[0] = true;
            inSet[0] = 1;    // Vertex 0 is in set U

            while (queue.Count > 0)
            {
                int t = queue.Dequeue();

                int[] neighbours = graph.GetNeighbours(t);
                foreach (int n in neighbours)
                {
                    if (visited[n] == false)
                    {
                        // put this vertex in opposite set as its parent.
                        if (inSet[t] == 1) inSet[n] = 2;
                        else inSet[n] = 1;

                        visited[n] = true;
                        queue.Enqueue(n);
                    }
                }
            }

            for (int i = 0; i < graph.NumberOfVertices; i++)
            {
                if (inSet[i] == 1) setU.Add(i);
                else if (inSet[i] == 2) setV.Add(i);
                else return false; // not a valid bipartite graph
            }

            return true;
        }
    }
}
