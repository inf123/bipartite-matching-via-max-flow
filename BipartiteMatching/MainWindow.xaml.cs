﻿// Christian Nilsson, AB4805
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace BipartiteMatching
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Graph graph1 = CreateGraph1();
            Graph graph2 = CreateGraph2();

            BipartiteMatch mf = new BipartiteMatch();

            string res1 = mf.FindBipartiteMatches(graph1);
            txtOutput.Text += Environment.NewLine + "Graph 1 Results:" + Environment.NewLine;
            txtOutput.Text += res1; // print result

            string res2 = mf.FindBipartiteMatches(graph2);
            txtOutput.Text += Environment.NewLine + "Graph 2 Results:" + Environment.NewLine;
            txtOutput.Text += res2; // print result

        }

        /// <summary>
        /// The graph in ProjektDA252A_MaxFlow.pdf
        /// </summary>
        /// <returns></returns>
        private Graph CreateGraph1()
        {
            Graph graph = new Graph(10);

            graph.AddUndirectedEdge(0, 5);
            graph.AddUndirectedEdge(0, 6);
            graph.AddUndirectedEdge(0, 8);

            graph.AddUndirectedEdge(1, 5);
            graph.AddUndirectedEdge(1, 6);
            graph.AddUndirectedEdge(1, 8);
            graph.AddUndirectedEdge(1, 9);

            graph.AddUndirectedEdge(2, 7);

            graph.AddUndirectedEdge(3, 7);

            graph.AddUndirectedEdge(4, 6);
            graph.AddUndirectedEdge(4, 7);
            graph.AddUndirectedEdge(4, 8);
            graph.AddUndirectedEdge(4, 9);

            Debug.WriteLine("Graph 1 Adjacency Matrix:");
            Debug.WriteLine(graph.getAdjacencyMatrixString());

            return graph;
        }

        /// <summary>
        /// Another graph.
        /// </summary>
        /// <returns></returns>
        private Graph CreateGraph2()
        {
            Graph graph = new Graph(12);

            graph.AddUndirectedEdge(0, 6);
            graph.AddUndirectedEdge(0, 8);
            graph.AddUndirectedEdge(1, 6);
            graph.AddUndirectedEdge(1, 9);
            graph.AddUndirectedEdge(2, 8);
            graph.AddUndirectedEdge(3, 7);
            graph.AddUndirectedEdge(3, 10);
            graph.AddUndirectedEdge(4, 9);
            graph.AddUndirectedEdge(3, 9);
            graph.AddUndirectedEdge(5, 8);
            graph.AddUndirectedEdge(5, 10);
            graph.AddUndirectedEdge(3, 11);

            Debug.WriteLine("Graph 2 Adjacency Matrix:");
            Debug.WriteLine(graph.getAdjacencyMatrixString());

            return graph;
        }

        
    }
}
